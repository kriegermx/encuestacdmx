function onLoadStats(){
    var requestNum = ["id=1886640","id=1886639","id=1886638"];
    var urlPreffix = "http://mexicocity2015.actionpath.org/responses.json?issue_";
    window.currentRequest = 0;
    for(var j = 0; j < requestNum.length; j++) {
        var urlFinal = urlPreffix + requestNum[j];

        $.getJSON(urlFinal, function (data) {
            if (window.currentRequest == 0)
                window.issueDataArray = [];
            window.issueDataArray.push(data);
            window.currentRequest++;
            if (window.currentRequest > 2) {
                getFlockTrackerData();
            }
        });
    }
}

function processJSON(tempJsonAns){
    var currentQuestion = new Object();
    window.numOfAnswers += tempJsonAns.responses.length; //summarize quantity of questions answered
    currentQuestion.title = tempJsonAns.issue.question;
    currentQuestion.type = tempJsonAns.issue.answerType;
    var answersArray = []; //made 3 times
    shortcutJsonResponses = tempJsonAns.responses;

    for(var n = 0; n < tempJsonAns.issue.answers.length; n++){ //repeats equal to amount of answer options
        var currentAnswer = new Object();
        currentAnswer.name = tempJsonAns.issue.answers[n]; //supposed to get the answer label at every iteration
        var counterForQuestion = 0;
        for(var m = 0; m < tempJsonAns.responses.length; m++){ //repeats equal to amount of responses objects
            if(currentAnswer.name != null && currentAnswer.name != ""){ //to avoid null or void answers
                if(currentAnswer.name == tempJsonAns.responses[m].answer)
                    counterForQuestion += 1;
            }
        }
        currentAnswer.counter = counterForQuestion;
        answersArray.push(currentAnswer);
    }

    if(true){
        currentAnswer = new Object();
        counterForQuestion = 0;
        for(var a = 0; a < tempJsonAns.responses.length; a++){
            if(tempJsonAns.responses[a].answer != "" && tempJsonAns.responses[a].answer != null){ //check if answer is not null nor void
                var revisor = tempJsonAns.issue.answers.length;
                for(var n = 0; n < tempJsonAns.issue.answers.length; n++){
                    if (tempJsonAns.issue.answers[n] != tempJsonAns.responses[a].answer){
                        revisor -= 1;
                    }
                }
                if(0 == revisor){
                    currentAnswer.name = "Otro";
                    counterForQuestion += 1;
                }
            }
        }

        if(0 < counterForQuestion){
            currentAnswer.counter = counterForQuestion;
            answersArray.push(currentAnswer);
        }
    }

    currentQuestion.answersArray = answersArray;
    return currentQuestion;
}

function processDataForWordCloud(input){
    var data = new Object();
    data.title = input.issue.question;
    data.type = input.issue.answerType;
    data.answersArray = input.wordArray;
    return data;
}

function getFlockTrackerData() {
    var urlAll = 'https://www.googleapis.com/fusiontables/v1/query?sql=SELECT%20*%20FROM%201zpX42TetzYiwPRXDRtFfEyzOzPsJMZpLL-XYSqzc&key=AIzaSyBuD0Br1NQHFep_iktaeCW1_DOxeq2t0zA';
    var urlHombres = 'https://www.googleapis.com/fusiontables/v1/query?sql=SELECT%20*%20FROM%201zpX42TetzYiwPRXDRtFfEyzOzPsJMZpLL-XYSqzc%20WHERE%20q6=%27Hombre%27%20&key=AIzaSyBuD0Br1NQHFep_iktaeCW1_DOxeq2t0zA';
    var urlMujeres = 'https://www.googleapis.com/fusiontables/v1/query?sql=SELECT%20*%20FROM%201zpX42TetzYiwPRXDRtFfEyzOzPsJMZpLL-XYSqzc%20WHERE%20q6=%27Mujer%27%20&key=AIzaSyBuD0Br1NQHFep_iktaeCW1_DOxeq2t0zA';

    $.getJSON(urlAll, function(data) {
        var finalJSON = {
            issues:[
                {
                    issue: {
                        id: 1886638,
                        question: "¿Cuál crees que es el principal problema de este jardín?",
                        answers:["Inseguridad","Basura","Falta de iluminación","Obstáculos para entrar al parque","Ninguno"],
                        answerType: "select1"
                    },
                    responses: [

                    ]
                },
                {
                    issue: {
                        id: 1886639,
                        question: "¿Qué es lo que más te gustaría que se hiciera en este jardín?",
                        answers: ["Actividades culturales","Poner mesas y sillas para comer","Acceso a internet público"],
                        answerType: "select1"
                    },
                    responses: [

                    ]
                },
                {
                    issue: {
                        id: 1886640,
                        question: "¿Por qué pasas por el jardín?",
                        answers: ["Vivo por aquí","Trabajo por aquí","Visito frecuentemente el jardín","Casi nunca paso por aquí"],
                        answerType: "select1"
                    },
                    responses: [

                    ]
                },
                {
                    issue: {
                        id: 1886641,
                        question: "¿Qué te hace sentir este lugar?",
                        answers: ["Estrés","Tristeza","Alegría","Tranquilidad"],
                        answerType: "select1"
                    },
                    responses: [

                    ]
                },
                {
                    issue: {
                        id: 1886642,
                        question: "¿Cuál es la primera palabra que te viene a la mente cuando piensas en este lugar?",
                        answerType: "text"
                    },
                    responses: [

                    ],
                    wordArray: [
                    ]
                },
                {
                    issue: {
                        id: 1886643,
                        question: "¿Alguna vez te han pedido tu opinión sobre algún espacio público?",
                        answers: ["Si","No"],
                        answerType: "select1"
                    },
                    responses: [

                    ]
                },
                {
                    issue: {
                        id: 1886644,
                        question: "Género",
                        answers: ["Hombre","Mujer"],
                        answerType: "select1"
                    },
                    responses: [

                    ]
                }
            ]
        }

        data.rows.forEach(function(row, index){
            var response = {
                id:'00'+index,
                install_id:'00'+index,
                issue_id:'',
                answer:row[11],
                timestamp:row[3],
                lat: row[4],
                lng: row[6],
                url: ''
            };
            finalJSON.issues[0].responses.push(response);
        });

        data.rows.forEach(function(row, index){
            var response = {
                id:'01'+index,
                install_id:'01'+index,
                issue_id:'',
                answer:row[12],
                timestamp:row[3],
                lat: row[4],
                lng: row[6],
                url: ''
            };
            finalJSON.issues[1].responses.push(response);
        });

        data.rows.forEach(function(row, index){
            var response = {
                id:'02'+index,
                install_id:'02'+index,
                issue_id:'',
                answer:row[13],
                timestamp:row[3],
                lat: row[4],
                lng: row[6],
                url: ''
            };
            finalJSON.issues[2].responses.push(response);
        });

        data.rows.forEach(function(row, index){
            var response = {
                id:'03'+index,
                install_id:'03'+index,
                issue_id:'',
                answer:row[14],
                timestamp:row[3],
                lat: row[4],
                lng: row[6],
                url: ''
            };
            finalJSON.issues[3].responses.push(response);
        });

        data.rows.forEach(function(row, index){
            var response = {
                id:'04'+index,
                install_id:'04'+index,
                issue_id:'',
                answer:row[15],
                timestamp:row[3],
                lat: row[4],
                lng: row[6],
                url: ''
            };
            finalJSON.issues[4].responses.push(response);
            row[15].length > 0 ? finalJSON.issues[4].wordArray.push(row[15]) : false;
        });

        data.rows.forEach(function(row, index){
            var response = {
                id:'05'+index,
                install_id:'05'+index,
                issue_id:'',
                answer:row[16],
                timestamp:row[3],
                lat: row[4],
                lng: row[6],
                url: ''
            };
            finalJSON.issues[5].responses.push(response);
        });

        data.rows.forEach(function(row, index){
            var response = {
                id:'06'+index,
                install_id:'06'+index,
                issue_id:'',
                answer:row[17],
                timestamp:row[3],
                lat: row[4],
                lng: row[6],
                url: ''
            };
            finalJSON.issues[6].responses.push(response);
        });

        window.numOfAnswers = 0;
        var issues = finalJSON.issues;
        var questions = [];
        for (var i = 0; i < issues.length; i++) {
            switch(issues[i].issue.answerType){
                case "select1":
                    for(var j=0; j<window.issueDataArray.length; j++){
                        if(window.issueDataArray[j].issue.id == issues[i].issue.id){
                            for(var k=0; k<window.issueDataArray[j].responses.length; k++)
                                issues[i].responses.push(window.issueDataArray[j].responses[k]);
                        }
                    }
                    var currentQuestion = processJSON(issues[i]);
                    questions.push(currentQuestion);
                    break;
                case "text":
                    var cloudData = processDataForWordCloud(issues[i]);
                    questions.push(cloudData);
                    break;
            }
        }
        generateStatsUI(numOfAnswers, questions);
    });
}