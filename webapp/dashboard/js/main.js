(function($) {
	initSkel();

	$(document).on("click", "nav-1", onMenuItem1Press);
	$(document).on("click", "nav-2", onMenuItem2Press);
	$(document).on("click", "nav-3", onMenuItem3Press);

    google.load('visualization', '1.0', {
        packages: ['corechart']
    });

    onMenuItem1Press();
})(jQuery);

function onMenuItemPress(i){
    if(window.currentElement != i) {
        var wrapper = $(".content-wrapper");
        var loadingHtml = '<div class="loading-wrapper"><img src="images/loader.svg"></div>';

        wrapper.html(loadingHtml);
        wrapper.scrollTop(0);
        var navItem = $("#nav-"+i);
        var title = navItem.clone().children().remove().end().text();
        navItem.addClass("selected");
        window.currentElement = i;
        $(".mdl-layout-title").html(title);

        switch (i){
            case 1:
                onLoadStats();
                break;
        }

        var layout = $(".layout");
        var drawer = $(".mdl-layout__drawer");
        if(layout.hasClass("is-small-screen"))
            $(drawer).removeClass("is-visible");
    }
}

function onMenuItem1Press(){
    onMenuItemPress(1);
}

function onMenuItem2Press(){
    onMenuItemPress(2);
}

function onMenuItem3Press(){
    onMenuItemPress(3);
}

function generateStatsUI(totalNumOfAnswers, questionArray){
    var totalNumOfQuestions = questionArray.length;
    var numOfParticipants = 0;
    for(var z=0; z<questionArray[2].answersArray.length; z++){
        numOfParticipants += questionArray[2].answersArray[z].counter;
    }
    var html = '<div><img class="banner" src="images/img_banner.jpg"/>';
    html += '<div class="inner-padding"><div class="row">' +
        '<div class="4u">' +
        '<div class="mdl-card mdl-shadow--2dp card statistic dark">' +
        '<h4 class="title">Preguntas</h4>' +
        '<div class="divider"></div>' +
        '<h3>'+totalNumOfQuestions+'</h3>' +
        '</div>' +
        '</div>' +
        '<div class="4u">' +
        '<div class="mdl-card mdl-shadow--2dp card statistic dark">' +
        '<h4 class="title">Participantes</h4>' +
        '<div class="divider"></div>' +
        '<h3>'+numOfParticipants+'</h3>' +
        '</div>' +
        '</div>' +
        '<div class="4u$">' +
        '<div class="mdl-card mdl-shadow--2dp card statistic dark">' +
        '<h4 class="title">Respuestas</h4>' +
        '<div class="divider"></div>' +
        '<h3>'+totalNumOfAnswers+'</h3>' +
        '</div>' +
        '</div>' +
        '</div>';

    html += '<h4 class="section-title with-margin-top">Gráficas</h4>' +
        '<div class="row">';

    window.graphContainers = [];
    window.questions = [];
    google.setOnLoadCallback(drawCharts);

    window.wordCloud = null;
    var elements = 0;
    for(var i=0; i<totalNumOfQuestions; i++) {
        if(questionArray[i].type == "text"){
            window.wordCloud = questionArray[i];
        }else{
            window.questions.push(questionArray[i]);
            var graphContainerId = "graphContainer-"+elements;
            window.graphContainers.push(graphContainerId);
            var title = questionArray[i].title;
            var index = elements+1;
            html += '<div class="6u'+(index%2==0 ? '$' : "")+' 12u$(small)">' +
                '<div class="mdl-card mdl-shadow--2dp card" onclick="drawCharts()">' +
                '<h4 class="title">'+title+'</h4>' +
                '<div class="graph-container" id="'+graphContainerId+'"><p>Cargando Gráfica</p></div>' +
                '</div>' +
                '</div>';
            elements++;
        }
    }
    html += '</div>';

    if(window.wordCloud != null){
        html += '<h4 class="section-title with-margin-top">'+window.wordCloud.title+'</h4>';
        html += '<div class="word-cloud"></div>';
    }

    html += '<h4 class="section-title with-margin-top">Área Geográfica</h4>';
    html += '<iframe width="100%" height="520" frameborder="0" src="https://flockmx.cartodb.com/viz/c002e10a-529a-11e5-bbfe-0e0c41326911/embed_map" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>';
    html += '</div></div>';

    var wrapper = $(".content-wrapper");
    wrapper.html(html);
    window.addEventListener('resize', function(event){
        drawCharts();
        startCloud();
    });
    startCloud();
}

function drawCharts() {
    for(var i=0; i<window.graphContainers.length; i++){
        var question = window.questions[i];
        var containerId = window.graphContainers[i];
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Answer');
        data.addColumn('number', 'Count');
        for(var x=0; x<question.answersArray.length; x++){
            data.addRows([
                [question.answersArray[x].name, question.answersArray[x].counter]
            ]);
        }

        var width = $('#'+containerId).parent().width();
        var options = {
            'width': width,
            'height': 400
        };

        var chart = new google.visualization.PieChart(document.getElementById(containerId));
        chart.draw(data, options);
    }
}

var layout;
var fill = d3.scale.category20();

function startCloud(){
    if(window.wordCloud != null){
        var cont = $('.word-cloud');
        var width = cont.width();
        var strs = [];

        var wordCounts = Object();
        window.wordCloud.answersArray.forEach(function(word){
            wordCounts[word] ? wordCounts[word] += 1 : wordCounts[word] = 1;
        });

        words = Object.keys(wordCounts).map(function(d) {
            return {text: d, size: 25 + wordCounts[d] * 5};
        });

        cont.html("");
        layout = d3.layout.cloud()
            .size([width, 500])
            .words(words)
            .padding(5)
            .rotate(function() { return ~~(Math.random() * 2) * 90; })
            .font("Roboto")
            .fontSize(function(d) { return d.size; })
            .on("end", draw);
        layout.start();
    }

}

function draw(words) {
    d3.select(".word-cloud").append("svg")
        .attr("width", layout.size()[0])
        .attr("height", layout.size()[1])
        .append("g")
        .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
        .selectAll("text")
        .data(words)
        .enter().append("text")
        .style("font-size", function(d) { return d.size + "px"; })
        .style("font-family", "Impact")
        .style("fill", function(d, i) { return fill(i); })
        .attr("text-anchor", "middle")
        .attr("transform", function(d) {
            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
        })
        .text(function(d) { return d.text; });
}